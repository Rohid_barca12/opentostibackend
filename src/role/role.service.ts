import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateRoleDto } from 'src/dto/create.role.dto';
import { Role } from 'src/entities/Role';
import { Repository } from 'typeorm';

@Injectable()
export class RoleService {
    constructor (
        @InjectRepository(Role)
        private readonly roleRepository: Repository<Role>
    ) {}

    getAll() {
        return this.roleRepository.find();
    }

    getByName(name: string) {
        return this.roleRepository.findOne({ name: name})
    }

    async create(dto: CreateRoleDto): Promise<Role> {
        const role: Role = await this.getByName(dto.name)

        if (role) {
            throw new BadRequestException('This role already exists')
        }

        return this.roleRepository.save(dto)
    }
}
