import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateRoleDto } from 'src/dto/create.role.dto';
import { RoleService } from './role.service';

@Controller('Role')
export class RoleController {
    constructor(
        private readonly roleService: RoleService
    ) {}

    @Get()
    getAll() {
        return this.roleService.getAll();
    }

    @Post()
    create (@Body() body: CreateRoleDto) {
        return this.roleService.create(body);
    }
}
