import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateProductDto {

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    price: string;

    @IsNumber()
    categoryId: number;

    @IsNumber()
    sizeId: number;

    @IsNumber()
    colorId: number;

    @IsNumber()
    genderId: number;
}