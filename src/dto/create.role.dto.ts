import { IsNotEmpty, isNumber } from "class-validator";

export class CreateRoleDto {
    @IsNotEmpty()
    name: string;
}
