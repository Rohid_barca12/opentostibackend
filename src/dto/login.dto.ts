import { MinLength, MaxLength, IsEmail } from 'class-validator'

export class LoginDto {
    
    @IsEmail()
    email: string;

    @MinLength(5)
    @MaxLength(16)
    password: string;
}
