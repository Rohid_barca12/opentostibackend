import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateBrandDto {
    @IsNotEmpty()
    name: string;
}