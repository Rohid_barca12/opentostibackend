import { IsNotEmpty, isNumber } from "class-validator";

export class CreateOrderDto {
    @IsNotEmpty()
    id: number;
}