import { IsHexColor, IsNotEmpty, isNumber } from "class-validator";

export class CreateColorDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    @IsHexColor()
    hex: string;
}