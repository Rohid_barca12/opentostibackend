import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateSizeDto {
    
    @IsNotEmpty()
    @IsNumber()
    eu: number;

    @IsString()
    @IsNotEmpty()
    international: string;

    @IsNumber()
    @IsNotEmpty()
    categoryId: number;
}