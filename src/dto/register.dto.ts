import { MinLength, MaxLength, IsEmail } from 'class-validator'

export class RegisterUserDto {

    @MinLength(2)
    @MaxLength(64)
    name: string;

    @IsEmail()
    email: string;

    @MinLength(6)
    @MaxLength(32)
    password: string;

    @MinLength(6)
    @MaxLength(32)
    confirmPassword: string;
}
