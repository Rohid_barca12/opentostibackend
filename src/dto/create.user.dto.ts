import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateUserDto {

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    address: string;

    @IsNumber()
    houseNumber: number;

    @IsNotEmpty()
    postcode: string;

    @IsNotEmpty()
    city: string;

    @IsNumber()
    phone_number?: number;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    password: string;

    @IsNumber()
    roleId: number;
}