import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateGenderDto } from 'src/dto/create.gender.dto';
import { GenderService } from './gender.service';

@Controller('Gender')
export class GenderController {
    constructor(
        private readonly genderService: GenderService
    ) {}

    @Get()
    getAll() {
        return this.genderService.getAll();
    }

    @Post()
    create (@Body() body: CreateGenderDto) {
        return this.genderService.create(body);
    }
}