import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateGenderDto } from 'src/dto/create.gender.dto';
import { Gender } from 'src/entities/Gender';
import { Repository } from 'typeorm';

@Injectable()
export class GenderService {
    constructor (
        @InjectRepository(Gender)
        private readonly genderRepository: Repository<Gender>
    ) {}

    getAll() {
        return this.genderRepository.find();
    }

    getByName(name: string) {
        return this.genderRepository.findOne({ name: name})
    }

    async create(dto: CreateGenderDto): Promise<Gender> {
        const gender: Gender = await this.getByName(dto.name)

        if (gender) {
            throw new BadRequestException('This gender already exists')
        }

        return this.genderRepository.save(dto)
    }
}