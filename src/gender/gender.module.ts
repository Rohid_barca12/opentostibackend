import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GenderController } from './gender.controller';
import { GenderService } from './gender.service';

// import entities
import { Gender } from 'src/entities/Gender';

@Module({
    imports: [TypeOrmModule.forFeature([Gender])],
    controllers: [GenderController],
    providers: [GenderService],
    exports: [GenderService]
})
export class GenderModule {}