import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateOrderDto } from 'src/dto/create.order.dto';
import { OrderService } from './order.service';

@Controller('Order')
export class OrderController {
    constructor(
        private readonly orderService: OrderService
    ) {}

    @Get()
    getAll() {
        return this.orderService.getAll();
    }

    @Post()
    create (@Body() body: CreateOrderDto) {
        return this.orderService.create(body);
    }
}