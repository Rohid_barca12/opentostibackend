import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateOrderDto } from 'src/dto/create.order.dto';
import { Order } from 'src/entities/Order';
import { Repository } from 'typeorm';

@Injectable()
export class OrderService {
    constructor (
        @InjectRepository(Order)
        private readonly orderRepository: Repository<Order>
    ) {}

    getAll() {
        return this.orderRepository.find();
    }

    getById(id: number) {
        return this.orderRepository.findOne({ id: id})
    }

    async create(dto: CreateOrderDto): Promise<Order> {
        const order: Order = await this.getById(dto.id)

        if (order) {
            throw new BadRequestException('This order already exists')
        }

        return this.orderRepository.save(dto)
    }
}