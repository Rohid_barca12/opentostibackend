import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateBrandDto } from 'src/dto/create.brand.dto';
import { Brand } from 'src/entities/Brand';
import { Repository } from 'typeorm';

@Injectable()
export class BrandService {
    constructor(
        @InjectRepository(Brand)
        private readonly brandRepository: Repository<Brand>
    ) {}

    getAll() {
        return this.brandRepository.find();
    }

    getByName(name: string) {
        return this.brandRepository.findOne({name: name })
    }

    async create(dto: CreateBrandDto): Promise<Brand> {
        const brand: Brand = await this.getByName(dto.name)

        if (brand) {
            throw new BadRequestException('this brand already exists')
        }

        return this.brandRepository.save(dto)
    }
}