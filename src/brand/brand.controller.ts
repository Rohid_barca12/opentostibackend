import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateBrandDto } from 'src/dto/create.brand.dto';
import { BrandService } from './brand.service';

@Controller()
export class BrandController {
    constructor(
        private readonly brandService: BrandService
    ) {}

    @Get()
    getAll() {
        return this.brandService.getAll();
    }

    @Post()
    create (@Body() body: CreateBrandDto) {
        return this.brandService.create(body)
    }
}