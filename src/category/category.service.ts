import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateCategoryDto } from 'src/dto/create.category.dto';
import { Category } from 'src/entities/Category';
import { Repository } from 'typeorm';

@Injectable()
export class CategoryService {
    constructor (
        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>
    ) {}

    getAll() {
        return this.categoryRepository.find();
    }

    getByName(name: string) {
        return this.categoryRepository.findOne({ name: name })
    }

    async create(dto: CreateCategoryDto): Promise<Category> {
        const category: Category = await this.getByName(dto.name)

        if (category) {
            throw new BadRequestException('This category already exists')
        }

        return this.categoryRepository.save(dto)
    }
}
