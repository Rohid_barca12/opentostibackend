import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateCategoryDto } from 'src/dto/create.category.dto';
import { CategoryService } from './category.service';

@Controller('category')
export class CategoryController {
    constructor(
        private readonly categoryService: CategoryService
    ) {}

    @Get()
    getAll()  {
        return this.categoryService.getAll();
    }

    @Post()
    create (@Body() body: CreateCategoryDto) {
        return this.categoryService.create(body);
    }
}
