import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from './user.service';


@Controller('user')
@UseGuards(AuthGuard('jwt'))
export class UserController {
    constructor(
        private readonly userService: UserService
    ) {}

    @Get()
    getAll()  {
        return this.userService.getAll();
    }
}
