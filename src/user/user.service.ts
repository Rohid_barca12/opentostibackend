import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/User';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
    constructor (
        @InjectRepository(User)
        private readonly userRepository: Repository<User>
    ) {}

    getAll() {
        return this.userRepository.find();
    }

    getByName(name: string) {
        return this.userRepository.findOne({ name: name })
    }

    getByEmail(email: string) {
        return this.userRepository.findOne({ email: email })
    }

    save(user: User): Promise<User> {
        return this.userRepository.save(user)
    }
}
