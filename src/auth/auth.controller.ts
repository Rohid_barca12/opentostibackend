import { Controller, Request, Post, UseGuards, Body } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { AuthService } from './auth.service'
import { RegisterUserDto } from 'src/dto/register.dto'
import { LoginDto } from 'src/dto/login.dto'

@Controller('auth')
export class AuthController {
  constructor (
    private readonly authService: AuthService
  ) {}

  /**
   * Logs a user in with username and password.
   * Does not require any authorization.
   *
   * @param req Request
   */
  @Post('login')
  async login (@Body() dto: LoginDto) {
    return this.authService.login(dto)
  }

  /**
   * Registers a new user with the register user dto.
   * Does not require any authorization.
   *
   * @param register RegisterUserDto
   */
  @Post('register')
  async register (@Body() register: RegisterUserDto) {
    return this.authService.register(register)
  }
}
