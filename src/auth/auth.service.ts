import { Injectable, BadRequestException } from '@nestjs/common'
import { UserService } from '../user/user.service'
import { JwtService } from '@nestjs/jwt'
import { User } from 'src/entities/User'
import * as bcrypt from 'bcrypt'
import { RegisterUserDto } from 'src/dto/register.dto'
import { LoginDto } from 'src/dto/login.dto'

@Injectable()
export class AuthService {
  constructor (
    private readonly userService: UserService,
    private readonly jwtService: JwtService
  ) {}

  async validateUser (name: string, pass: string): Promise<any> {
    const user: User = await this.userService.getByName(name)

    if (!user) {
      return null
    }

    const resultBcrypt: boolean = await bcrypt.compare(pass, user.password)

    if (!resultBcrypt) {
      return null
    }

    const { password, ...result } = user
    return result
  }

  async login (dto: LoginDto) {
    const user: User = await this.userService.getByEmail(dto.email)

    // Check if user with email exists.
    if (!user) {
      return null
    }

    const resultBcrypt: boolean = await bcrypt.compare(dto.password, user.password)

    if (!resultBcrypt) {
      return null
    }

    return {
      access_token: this.jwtService.sign({
        email: user.email,
        sub: user.id
      })
    }
  }

  async register (register: RegisterUserDto) {
    let user: User = await this.userService.getByEmail(register.email)

    // Check if somebody has this email in use.
    if (user) {
      throw new BadRequestException('Email is already in use by another user.')
    }

    // Check if password is the same as confirm password.
    if (register.password !== register.confirmPassword) {
      throw new BadRequestException('Passwords do not match!')
    }

    register.password = await bcrypt.hash(register.password, 12)
    this.userService.save({
      name: register.name,
      email: register.email,
      password: register.password
    })
  }
}
