import {PrimaryGeneratedColumn, Entity, Column, OneToMany } from 'typeorm';
import { Product } from './Product';

@Entity('brand')
export class Brand {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name: string;

    @OneToMany(type => Product, product => product.brand)
    product?: Product[];
}