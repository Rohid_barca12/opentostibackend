import { PrimaryGeneratedColumn, Entity, Column, OneToMany, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';
import { Category } from './Category';
import { Product } from './Product';
import { SizeRow } from './sizeRow';

@Entity('size')
export class Size {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: "int" })
    eu: number;

    @Column('varchar', { length: 3 })
    international: string;

    @ManyToOne(type => Category, category => category.name)
    category?: Category;

    @Column()
    categoryId: number;

    @OneToMany(type => SizeRow, sizeRow => sizeRow.size)
    sizeRow?: SizeRow[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}