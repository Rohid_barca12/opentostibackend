import { PrimaryGeneratedColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { User } from './User';

@Entity('role')
export class Role {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name: string;

    @OneToMany(type => User, user => user.role)
    user?: User[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}