import { PrimaryGeneratedColumn, Entity, Column, OneToMany  } from 'typeorm';
import { Product } from './Product';

@Entity('gender')
export class Gender {
    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name: string;

    @OneToMany(type => Product, product => product.gender)
    product?: Product[];
}