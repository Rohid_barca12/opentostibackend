import { PrimaryGeneratedColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany } from 'typeorm';
import { Category } from './Category';
import { OrderRow } from './OrderRow';
import { Size } from './Size';
import { SizeRow } from './sizeRow';
import { Color } from './Color';
import { Gender } from './Gender';
import { Brand } from './Brand';

@Entity('product')
export class Product {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name: string;

    @Column('varchar')
    price: string;

    @ManyToOne(type => Category, category => category.product)
    category?: Category;

    @Column()
    categoryId: number;

    @ManyToOne(type => Color, color => color.product)
    color?: Color;

    @Column()
    colorId: number;

    @ManyToOne(type => Gender, gender => gender.product)
    gender?: Gender;

    @Column()
    genderId: number;

    @ManyToOne(type => Brand, brand => brand.product)
    brand?: Brand;

    @Column()
    brandId?: number;

    @OneToMany(type => OrderRow, orderRow => orderRow.product)
    orderRow?: OrderRow[];

    @OneToMany(type => SizeRow, sizeRow => sizeRow.product)
    sizeRow?: SizeRow[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}