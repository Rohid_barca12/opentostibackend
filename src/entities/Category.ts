import { PrimaryGeneratedColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { Product } from './Product';
import { Size } from './Size';

@Entity('category')
export class Category {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name: string;

    @OneToMany(type => Product, product => product.category)
    product?: Product[];

    @OneToMany(type => Size, size => size.category)
    size?: Size[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}