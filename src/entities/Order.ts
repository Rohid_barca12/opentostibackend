import { PrimaryGeneratedColumn, Entity, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany } from 'typeorm';
import { OrderRow } from './OrderRow';
import { User } from './User';

@Entity('order')
export class Order {

    @PrimaryGeneratedColumn()
    id?: number;
    
    @ManyToOne(type => User, user => user.order)
    user?: User;

    @OneToMany(type => OrderRow, orderRow => orderRow.order)
    orderRow?: OrderRow[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}