import { PrimaryGeneratedColumn, Entity, Column, OneToMany  } from 'typeorm';
import { Product } from './Product';

@Entity('color')
export class Color {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name: string;

    @Column('varchar')
    hex: string;

    @OneToMany(type => Product, product => product.color)
    product?: Product[];
}