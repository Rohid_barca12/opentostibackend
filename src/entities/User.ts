import { PrimaryGeneratedColumn, Entity, Column, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne } from 'typeorm';
import { Order } from './Order';
import { Role } from './Role';

@Entity('user')
export class User {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column('varchar')
    name: string;

    @Column('varchar', {  nullable: true })
    address?: string;

    @Column('varchar', { nullable: true })
    houseNumber?: number;

    @Column('varchar', { nullable: true })
    postalCode?: string;

    @Column('varchar', { nullable: true })
    city?: string;

    @Column('varchar', { nullable: true })
    phoneNumber?: string;

    @Column('varchar')
    email: string;

    @Column('varchar')
    password: string;

    @ManyToOne(type => Role, role => role.user)
    role?: Role;

    @OneToMany(type => Order, order => order.user)
    order?: Order[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}