import { PrimaryGeneratedColumn, Entity, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';
import { Order } from './Order';
import { Product } from './Product';

@Entity('orderrow')
export class OrderRow {

    @PrimaryGeneratedColumn()
    id?: number;
    
    @ManyToOne(type => Order, order => order.orderRow)
    order?: Order;

    @ManyToOne(type => Product, product => product.orderRow)
    product?: Product;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}