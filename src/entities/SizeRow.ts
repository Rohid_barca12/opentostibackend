import { PrimaryGeneratedColumn, Entity, CreateDateColumn, UpdateDateColumn, ManyToOne, Column } from 'typeorm';
import { Product } from './Product';
import { Size } from './Size';

@Entity('sizerow')
export class SizeRow {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column({ type: 'int' })
    amount: number;
    
    @ManyToOne(type => Size, size => size.sizeRow)
    size?: Size;

    @ManyToOne(type => Product, product => product.sizeRow)
    product?: Product;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt?: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt?: Date;
}