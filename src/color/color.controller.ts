import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateColorDto } from 'src/dto/create.color.dto';
import { ColorService } from './color.service';

@Controller('Color')
export class ColorController {
    constructor(
        private readonly colorService: ColorService
    ) {}

    @Get()
    getAll()  {
        return this.colorService.getAll();
    }

    @Post()
    create (@Body() body: CreateColorDto) {
        return this.colorService.create(body);
    }
}

