import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateColorDto } from 'src/dto/create.color.dto';
import { Color } from 'src/entities/Color';
import { Repository } from 'typeorm';

@Injectable()
export class ColorService {
    constructor (
        @InjectRepository(Color)
        private readonly colorRepository: Repository<Color>
    ) {}

    getAll() {
        return this.colorRepository.find();
    }

    getByName(name: string) {
        return this.colorRepository.findOne({ name: name });
    }

    getByHex(hex: string) {
        return this.colorRepository.findOne({ hex: hex });
    }

    async create(dto: CreateColorDto): Promise<Color> {
        let color: Color = await this.getByName(dto.name);

        if (color) {
            throw new BadRequestException('This color name is already in use');
        }

        color = await this.getByHex(dto.hex);

        if (color) {
            throw new BadRequestException('This hex color code is already in use');
        }

        return this.colorRepository.save(dto);
    }
}
