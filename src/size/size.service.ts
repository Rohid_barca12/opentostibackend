import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateSizeDto } from 'src/dto/create.size.dto';
import { Size } from 'src/entities/Size';
import { Repository } from 'typeorm';

@Injectable()
export class SizeService {
    constructor (
        @InjectRepository(Size)
        private readonly sizeRepository: Repository<Size>
    ) {}

    getAll() {
        return this.sizeRepository.find();
    }

    getByEuSize(size: number) {
        return this.sizeRepository.findOne({ eu: size })
    }

    getByInternationalSize(size: string) {
        return this.sizeRepository.findOne({ international: size })
    }

    async create(dto: CreateSizeDto): Promise<Size> {
        const size: Size = await this.sizeRepository.findOne({
            eu: dto.eu,
            international: dto.international,
            categoryId: dto.categoryId
        })

        if (size) {
            throw new BadRequestException('This size already exists')
        }

        return this.sizeRepository.save(dto)
    }
}
