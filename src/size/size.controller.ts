import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateSizeDto } from 'src/dto/create.size.dto';
import { SizeService } from './size.service';

@Controller('size')
export class SizeController {
    constructor(
        private readonly sizeService: SizeService
    ) {}

    @Get()
    getAll()  {
        return this.sizeService.getAll();
    }

    @Post()
    create (@Body() body: CreateSizeDto) {
        return this.sizeService.create(body);
    }
}
