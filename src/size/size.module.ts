import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SizeController } from './size.controller';
import { SizeService } from './size.service';

// import entities
import { Size } from 'src/entities/Size';

@Module({
    imports: [TypeOrmModule.forFeature([Size])],
    controllers: [SizeController],
    providers: [SizeService],
    exports: [SizeService]
})
export class SizeModule {}
