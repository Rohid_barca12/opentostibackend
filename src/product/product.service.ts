import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductDto } from 'src/dto/create.product.dto';
import { Product } from 'src/entities/Product';
import { Repository } from 'typeorm';

@Injectable()
export class ProductService {
    constructor (
        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>
    ) {}

    getAll() {
        return this.productRepository.find();
    }

    getById(id: number) {
        return this.productRepository.findOne(id)
    }

    getByCategoryId(id: number) {
        return this.productRepository.find({ categoryId: id })
    }

    // getBySizeId(id: number) {
    //     return this.productRepository.find({ sizeId: id })
    // }

    getByName(name: string) {
        return this.productRepository.findOne({ name: name })
    }

    async create(dto: CreateProductDto): Promise<Product> {
        const product: Product = await this.getByName(dto.name)

        if (product) {
            throw new BadRequestException('This product already exists')
        }

        return this.productRepository.save(dto)
    }
}
