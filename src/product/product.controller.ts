import { Body, Controller, Get, Param, Post, Req } from '@nestjs/common';
import { CreateProductDto } from 'src/dto/create.product.dto';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
    constructor(
        private readonly productService: ProductService
    ) {}

    @Get()
    getAll()  {
        return this.productService.getAll();
    }

    @Get(':id')
    getById(@Param('id') productId: number) {
        return this.productService.getById(productId)
    }

    @Get('category/:id')
    getByCategoryId(@Param('id') categoryId: number) {
        return this.productService.getByCategoryId(categoryId)
    }

    // @Get('size/:id')
    // getBySizeId(@Param('id') sizeId: number) {
    //     return this.productService.getBySizeId(sizeId)
    // }

    @Post()
    create (@Body() body: CreateProductDto) {
        return this.productService.create(body);
    }
}
