import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';

// import entities
import { Product } from 'src/entities/Product';

// import modules
import { CategoryModule } from 'src/category/category.module';
import { SizeModule } from 'src/size/size.module';

@Module({
    imports: [
        CategoryModule,
        SizeModule,
        TypeOrmModule.forFeature([Product])],
    controllers: [ProductController],
    providers: [ProductService],
    exports: [ProductService]
})
export class ProductModule {}
