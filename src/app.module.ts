import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { CategoryModule } from './category/category.module';
import { ColorModule } from './color/color.module';

// import entities
import { Category } from './entities/Category';
import { Color } from './entities/Color';
import { Gender } from './entities/Gender';
import { Brand } from './entities/Brand';
import { Order } from './entities/Order';
import { OrderRow } from './entities/OrderRow';
import { Product } from './entities/Product';
import { Role } from './entities/Role';
import { Size } from './entities/Size';
import { SizeRow } from './entities/sizeRow';
import { User } from './entities/User';
import { GenderModule } from './gender/gender.module';
import { OrderModule } from './order/order.module';

// import modules
import { ProductModule } from './product/product.module';
import { SizeModule } from './size/size.module';
import { UserModule } from './user/user.module';
import { RoleModule } from './role/role.module';
import { BrandModule } from './brand/brand.module';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'tosti',
      entities: [User, Product, Color, Order, OrderRow, Category, Role, Size, SizeRow, Gender, Brand],
      synchronize: true,
    }),

    UserModule,
    ProductModule,
    ColorModule,
    OrderModule,
    CategoryModule,
    SizeModule,
    GenderModule,
    RoleModule,
    BrandModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

